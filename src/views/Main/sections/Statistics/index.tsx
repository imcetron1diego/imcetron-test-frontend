import { device } from 'helpers/media-screen'
import styled from 'styled-components'

const StatisticsSection = styled.section`
  height: 100vh;
  width: 100%;
  padding: 2rem;
  .statisticsBackground {
    width: 100%;
    height: 100%;
    padding: 2rem 5rem;
    background: url('https://i.ibb.co/kgFkbSy/motor-winding-03-mod-2.png');
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    color: white;
    & > div {
      b {
        font-size: 4rem;
        color: white;
      }
      p {
        font-size: 1.75rem;
        max-width: 320px;
        font-weight: 600;
      }
    }
  }
  @media ${device.laptopL} {
    .statisticsBackground {
      justify-content: center;
      gap: 2rem;
      & > div {
        p {
          max-width: 22rem;
          font-size: 1.5rem;
        }
        }
      }
    }
  }

  @media ${device.tablet} {
    .statisticsBackground {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr;
      grid-template-rows: 1fr;
      padding: 2rem;
      align-items: end;
      gap: .5rem;
      & > div {
        text-align: center;
        b {
          font-size: 3rem;
        }
        p {
          font-size: 14px;
        }
        }
      }
    }
  }
  @media screen and (max-width: 650px) {
    .statisticsBackground {
      padding: 2rem;
      gap: .5rem;
      & > div {
        text-align: center;
        b {
          font-size: 2rem;
        }
        p {
          font-size: 12px;
        }
      }
    }
  }
  @media ${device.mobile} {
    height: 80vh;
    .statisticsBackground {
      & > div {
        text-align: center;
        b {
          font-size: 24px;
        }
        p {
          font-size: 11px;
        }
      }
    }
  }
`

const Statistics = () => {
  return (
    <StatisticsSection className="statisticsContainer">
      <div className="statisticsBackground">
        <div>
          <b data-aos="fade-up">300+</b>
          <p>clientes satisfechos por nuestra atención</p>
        </div>
        <div>
          <b data-aos="fade-up">25</b>
          <p>años de experiencia en el mercado</p>
        </div>
        <div>
          <b data-aos="fade-up">200</b>
          <p>productos importados y comercializados</p>
        </div>
      </div>
    </StatisticsSection>
  )
}

export default Statistics

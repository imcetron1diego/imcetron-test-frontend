export const size = {
  mobileS: 320,
  mobileM: 375,
  mobile: 512,
  tablet: 768,
  laptopS: 960,
  laptopM: 1280,
  laptopL: 1620,
  laptop: 1500,
  desktopL: 2000,
  desktop: 2560
}

export const device = {
  mobileS: `screen and (max-width: ${size.mobileS}px)`,
  mobileM: `screen and (max-width: ${size.mobileM}px)`,
  mobile: `screen and (max-width: ${size.mobile}px)`,
  tablet: `screen and (max-width: ${size.tablet}px)`,
  laptopS: `screen and (max-width: ${size.laptopS}px)`,
  laptopM: `screen and (max-width: ${size.laptopM}px)`,
  laptopL: `screen and (max-width: ${size.laptopL}px)`,
  laptop: `screen and (max-width: ${size.laptop}px)`,
  desktopL: `screen and (max-width: ${size.desktopL}px)`,
  desktop: `screen and (max-width: ${size.desktop}px)`,
};
import styled from 'styled-components'
import { Button, Select } from 'components'
import { FC, useState } from 'react'
import { device } from 'helpers/media-screen'
import { CloseOutlined, PlusOutlined } from '@ant-design/icons'

const HeaderProductsContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 2rem;
  .menu-header-container {
    display: none;
  }
  .menu-dropd {
    position: fixed;
    z-index: 6;
    background: white;
    width: 100%;
    height: calc(100vh - 2rem);
    top: 2rem;
    left: 0;
    box-shadow: 0 0 1px 200px rgb(0, 0, 0, 0.4);
    overflow: auto;
    padding: 2rem 2rem 2rem;
  }
  .bntAplyContainer {
    width: 100%;
    display: flex;
    justify-content: center;
    * {
      width: 100%;
      max-width: 300px;
    }
  }
  .filterByBrand {
    display: flex;
    flex-direction: column;
    gap: 1.5rem;
    width: 100%;
  }
  h2 {
    font-weight: 600;
    font-size: 2rem;
  }
  @media ${device.tablet} {
    display: grid;
    grid-template-colums: 1fr;
    gap: 2rem;
    .select-one {
      display: none;
    }
    .menu-header-container {
      display: grid;
      grid-template-columns: 1fr 1fr;
      gap: 1.5rem;
    }
  }
  .closeButton {
    display: flex;
    justify-content: end;
    font-size: 24px;
    width: 100%;
  }
`
interface HeaderProps {
  onChange: React.ChangeEventHandler<HTMLSelectElement> | undefined
  children: JSX.Element
}

const HeaderProducts: FC<HeaderProps> = (props) => {
  const { onChange, children } = props
  const [showMenu, setShowMenu] = useState(false)
  const displayMenu = () => {
    setShowMenu(!showMenu)
  }
  return (
    <HeaderProductsContainer className="headerProducts">
      <h2>Catálogo de Productos</h2>
      <Select onChange={onChange} className="select-one" />
      <div className="menu-header-container">
        {showMenu && (
          <div className="menu-dropd">
            <div onClick={displayMenu} className="closeButton">
              <CloseOutlined style={{ color: 'black' }} />
            </div>
            {children}
          </div>
        )}
        <Button onClick={displayMenu} type="button" buttonType="shadow">
          <p>Filtros</p>
        </Button>
        <Select onChange={onChange} />
      </div>
    </HeaderProductsContainer>
  )
}

export default HeaderProducts
